<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('confirm_password')->nullable()->after('password');
            $table->string('confirm_email_address')->nullable()->after('email'); 
            $table->string('country')->nullable()->after('settings');  
            $table->string('street_address')->nullable()->after('settings'); 
            $table->string('contact_number')->nullable()->after('street_address'); 
            $table->string('postal_code')->nullable()->after('contact_number');
            $table->string('store_id')->nullable()->after('postal_code');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('confirm_password');
            $table->dropColumn('confirm_email_address');
            $table->dropColumn('country');
            $table->dropColumn('street_address');
            $table->dropColumn('contact_number');
            $table->dropColumn('postal_code');
            $table->dropColumn('store_id');
        });
    }
}
